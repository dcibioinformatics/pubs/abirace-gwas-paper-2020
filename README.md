# Project
A Prospective Trial of Abiraterone Acetate plus Prednisone in Black and White Men with Metastatic Castrate-Resistant Prostate Cancer

# Prerequisites

The following files are needed per request to run the analysis script:
1. HapMap3 Build37
2. Experimental and clinical files
3. Pre-calculated global and local ancestry files

# Run
Per prerequistes files provided:
```shell
git clone git@gitlab.oit.duke.edu:dcibioinformatics/pubs/abirace-gwas-paper-2020.git
make
```
